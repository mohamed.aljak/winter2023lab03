public class Refrigirator
{
    public String brand;
    public int contents;
    public int maxCapacity;

    public void increaseContents()
    {
        if(this.contents < maxCapacity)
        {
            this.contents++;
        }
    }
    public void decreaseContents()
    {
        if(this.contents != 0)
        {
            this.contents--;
        }
    }

}
