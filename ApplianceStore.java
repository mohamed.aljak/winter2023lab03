import java.util.Scanner;

public class ApplianceStore
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        Refrigirator[] refrigirators = new Refrigirator[4];
        for(int i=0;i<4;i++)
        {
             refrigirators[i] = new Refrigirator();
             System.out.println("brand");
             refrigirators[i].brand = scanner.next();
             System.out.println("max capacity");
             refrigirators[i].maxCapacity = scanner.nextInt();
			 scanner.nextLine();
             System.out.println("contents");
             refrigirators[i].contents = scanner.nextInt();
			 scanner.nextLine();
        }
		System.out.println("Printing: Last");
        System.out.println(refrigirators[3].brand);
        System.out.println(refrigirators[3].maxCapacity);
        System.out.println(refrigirators[3].contents);

        refrigirators[0].increaseContents();
        refrigirators[0].increaseContents();
        refrigirators[0].decreaseContents();

		System.out.println("Printing: First");
        System.out.println(refrigirators[0].brand);
        System.out.println(refrigirators[0].maxCapacity);
        System.out.println(refrigirators[0].contents);
    }
}